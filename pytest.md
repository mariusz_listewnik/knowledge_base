# Pytest

## Runing tests

To run pytest, use:

python -m pytest 

-m flag is recommended. 


To get more detail when running tests:

`python -m pytest -v` verbose mode


## Useful command line options

`pytest -x           # stop after first failure
pytest --maxfail=2  # stop after two failures`

`pytest -m [mark_name]` runs tests only on specific marks. 

`pytest -k [keyword]` checks for keyword occurence "that match the given string expression (case-insensitive)" 

`pytest test_mod.py::test_func` runs a single  test from a specific module
`pytest test_mod.py::TestClass::test_method` another example

`pytest -ra` -r  provides a more detailed report of the results and can be
modified by different chars. With'`a` you get details of all tests except
passes.

For ease of use, if we use some command line options with all tests, they can
be specified in the configuration file (pyproject.toml or ptest.ini) 

Example from pyetest.ini:
`addopts = --maxfail=2 -rf  # exit after 2 failures, report fail info`


All command line options are available via `pytest --help`


