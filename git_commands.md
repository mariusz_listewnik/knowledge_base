# Git commands cheatsheet


## ALIASES

Help to shorten commands. Commands can also be coupled together into a
sequence.

Example:
`git config --global alias.ac '!git add -A && git commit -m'`

So this example allows to type `git ac 'Message for commit'`

Pretty cool

I added my own alias to push changes and to  a new remote branch

It goes `git pbo new_remote_branch`

pbo for 'push branch to origin'


## RESETTING CHANGES AKA UNSTAGING

Clean up your working tree, forget about changes made

`git reset --hard`

I think `HEAD^` also needs to be added at the end - will verify



Clean up the commit you last made, but keep the changes in staging


`git reset --soft HEAD^`


## RENAMING A RECENT COMMIT MESSAGE

`git commit --amend -m "New commit message."`


## FORCING TO UPDATE THE HISTORY ON A BRANCH

`git push --force branch-name`



## GIT TAG


Adding tags (locally first) - examples:

`git tag -a v0.4.0 -m "release v0.4.0"`
`git tag -a v1.0.0 -m "release v1.0.0"`

check exsting tags with

`git tag --list`

Remember to create and push tags from the spio-wyin repo

Remmber to ALWAYS GIT PULL ORIGIN MASTER beforehand!!! 


## PUSH TAGS:

`git push --tags`



I messed up and pushed wrong tag - can I remove it?

`git push --delete origin <tagname>`




## SHOW REMOTE LINKS


`git remote`

You can also specify -v, which shows you the URLs that Git has stored for the shortname to be used when reading and writing to that remote:

`git remote -v`
origin	https://github.com/schacon/ticgit (fetch)
origin	https://github.com/schacon/ticgit (push)



## How to check my configuration (e-mail, username etc)



The command git config --list will list the settings. There you should also find user.name and user.email.



## REMOVE BRANCH IN GIT



git branch -d <local-branch>


**Remove a remote branch**

`git push origin --delete <remote-branch-name>`



## Adding files in Git

`git add .` - add all changes from working trees. It does not cover the changes made by `git add -u`

`git add -u` 

Looks at changes to files and updates the list of files to stage

`git add -A`

Adds ALL changes, that is, it covers both `git add .`  and `git add -u`

Source: https://fix.code-error.com/difference-between-git-add-a-and-git-add/


