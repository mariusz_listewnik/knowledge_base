# Useful commands in Linux

## Renaming file:

mv oldname newname

Advance cases of renaming: https://www.hostinger.com/tutorials/how-to-rename-files-in-linux/


`sudo apt update` explained

https://embeddedinventor.com/sudo-apt-update-command-explained-for-beginners/


## Symlinks

Link to a folder

`ln -s /home/james james`

Result: you can access the previous folder by typing:

`cd james`
AWESOME!!

More info on how to link files, unlink etc.
https://www.freecodecamp.org/news/symlink-tutorial-in-linux-how-to-create-and-remove-a-symbolic-link/
