# PYTHON USEFUL INFORMATION AND COMMANDS
 
## Virtual envs
 
`python -m venv ./venv`


But recommended is with python3 if we're using new versions of Python:  
`python3 -m venv ./venv`  if using python3 command


Setting up a virtual environment when working on a project

1. One way is to set it up in the project folder folder - as shown above 


2. The other way is to have it separated from your project in a special folder on the system and then we just
activate the venv when needed

Example:
VENV_DIR="$HOME/.my_virtualenvs/project_name"
mkdir -p "${VENV_DIR}"
python3 -m venv "${VENV_DIR}"

Activate: source "${VENV_DIR}/bin/activate"

Second way is used by plugins like virtualenvwrapper

