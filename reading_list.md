# READING LIST

Here is a list of resources that I covered, with an indication if the subject
was more or less exhausted and is well-trained.

## Python generators:
https://realpython.com/introduction-to-python-generators/ - I read 70% but did
not check out the code examples myself

Dataset: https://realpython.com/bonus/generators-yield/


## Python Decorators

https://realpython.com/primer-on-python-decorators/

Not exhausted - there is plenty more in the primer to read. I worked on the
examples and they are stored in the learn_decorators branch (local).

## Pytest fixtures

https://docs.pytest.org/en/stable/fixture.html#safe-teardowns

Enormous resource. I've read more than half, but haven't tried code examples myself - it would be a good
idea to test at least some of the examples.


##  Pytest API reference

https://docs.pytest.org/en/stable/reference.html#ini-options-ref

I just discovered this page and there's a lot to read. I'm planning to skim
through first.

##  Video about leveraging Vim

https://github.com/mrl5/vim-of-mine - link in Readme.


## Grep + sed

Very interesting breakdown. I'd like to master these commands one by one

https://www.internalpointers.com/post/linux-find-and-replace-text-multiple-files

