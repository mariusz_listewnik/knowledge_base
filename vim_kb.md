# VIM commands

## DELETING

Delete a Single Line

1. Press Esc to navigate through the file easily.

2. Move the cursor to the line you want to delete.

2. Delete the line by pressing one of the following commands:

dd

D

If you need to delete content from a specific point in line forward, then use:
1D - to delete rest of line INCLUDING current character
ld$ - to delete rest of line EXCLUDING current character

Delete a Single Word

1. Start by moving into command mode with Esc.

2. Place the cursor at the beginning of the word you want to remove.

3. Press the keys:

dw



## Delete Multiple Lines

1. Press Esc.

2. Move the cursor to the first line you want to remove.

3. Use one of the following commands (replacing [#] with the number of lines):

[#]dd

d[#]d




## INSERT MODE:


i 


## Save changes

:w save

:wq save and exit





## How to copy text?

NORMAL MODE 


Copying (Yanking)

To copy text, place the cursor in the desired location and press the y key followed by the movement command. Below are some helpful yanking commands:

    yy - Yank (copy) the current line, including the newline character.
    3yy - Yank (copy) three lines, starting from the line where the cursor is positioned.
    y$ - Yank (copy) everything from the cursor to the end of the line.
    y^ - Yank (copy) everything from the cursor to the start of the line.
    yw - Yank (copy) to the start of the next word.
    yiw – Yank (copy) the current word.
    y% - Yank (copy) to the matching character. By default supported pairs are (), {}, and []. Useful to copy text between matching brackets
    
    
Pasting (Putting)

To put the yanked or deleted text, move the cursor to the desired location and press p to put (paste) the text after the cursor or P to put (paste) before the cursor.


VISUAL MODE

    Place the cursor on the line you want to begin copping or cutting.

    The visual mode has three subtypes.
        Press v to enter the visual mode.
        Press V to enter visual line mode, where the text is selected by line.
        Press Ctrl+v to enter visual block mode. In this mode, the text is selected by rectangle blocks.

    Entering the visual mode also marks a starting selection point.

    Move the cursor to the end of the text you want to copy or cut. You can use a movement command or up, down, right, and left arrow keys.
    Vim Copy, Cut and Paste in Visual Mode

    Press y to copy, or d to cut the selection.

    Move the cursor to the location where you want to paste the contents.

    Press P to paste the contents before the cursor, or p to paste it after the cursor.


---------------


## Redo Changes in Vim / Vi

The redo feature reverses the action of undo.

To redo a change in Vim and Vi use the Ctrl-R or :redo:

    Press the Esc key to go back to the normal mode.
    Use Ctrl-R (press and hold Ctrl and press r) to redo the last change. In Vim, you can also use quantifiers. For example, if you want to redo the 4 last changes, you would type 4Ctrl-R.

Each undo command can be reversed by a redo command.

More info about copying/pasting: https://phoenixnap.com/kb/cut-copy-paste-vim


----------------------

## PLUGINS

* NERDtree:

When in Vim, type :NERDtree and you will jump straight into the tree view.



## SEARCHING

Type `/` or `?` and in normal mode, then type keyword to search for. Iterate
through list with `n` (forward) and `N` backward.

More info: https://linuxize.com/post/vim-search/
